Known Issues:

-Some URL symbols are still leading to exceptions. ( | is one)


What remains to be done:

-Since the update to HttpComponents 4.3 many things are deprecated. 
Should be resolved.
-Until know the page content is handled by passing a String variable. 
I'm planning to switch to streams.
-Moving away from JSoup as Tag handler. Integrate Tika instead.