package com.itiniu.iticrawler.behaviors.inte;

public interface IScheduleUrlBehavior
{
	public boolean shouldScheduleURL(String inUrl);
}
