package com.itiniu.iticrawler.factories.inte;

import com.itiniu.iticrawler.livedatastorage.inte.IRobotTxtStore;

public interface IRobotTxtStorageFactory
{
	public IRobotTxtStore getRobotTxtData();
}
