package com.itiniu.iticrawler.factories.inte;

import com.itiniu.iticrawler.livedatastorage.inte.IScheduledURLStore;


public interface IScheduledURLStorageFactory
{
	public IScheduledURLStore getScheduledUrlData();
}
