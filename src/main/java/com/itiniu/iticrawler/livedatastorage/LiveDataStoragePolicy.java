package com.itiniu.iticrawler.livedatastorage;

public enum LiveDataStoragePolicy
{
	inMemory,
	cluster,
	toFile,
	toDB,
	custom;
}
